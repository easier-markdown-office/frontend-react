import React from 'react';
import MarkdownView from 'react-showdown';

function MarkdownPreview() {
  const text = '# hello, markdown!';

  return (
    <div>
      <MarkdownView markdown={text} options={{ tables: true, emoji: true }} />
    </div>
  );
}

export default MarkdownPreview;
