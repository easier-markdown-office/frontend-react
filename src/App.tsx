import React, { useEffect, useState } from 'react';
import './App.css';
import openSocket from 'socket.io-client';
import SplitPane from 'react-split-pane';
import TextEditor from './components/text-editor/TextEditor';
import MarkdownPreview from './components/markdown-preview/MarkdownPreview';

function App() {
  const [response, setResponse] = useState('');
  const ENDPOINT = 'http://localhost:3737';

  useEffect(() => {
    const socket = openSocket(ENDPOINT, {
      transports: ['websocket', 'polling'],
    });

    socket.on('views', (data: any) => {
      setResponse(data);
    });
  }, []);

  return (
    <SplitPane split="vertical" defaultSize={400} primary="first">
      <div>{response}</div>
      <div>
        <SplitPane split="vertical" defaultSize={400} primary="first">
          <div>
            <TextEditor />
          </div>
          <div>
            <MarkdownPreview />
          </div>
        </SplitPane>
      </div>
    </SplitPane>
  );
}

export default App;
